﻿using RelocalizeService.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Tridion.ContentManager;
using Tridion.ContentManager.CoreService.Client;
using Tridion.Practice;

namespace RelocalizeService.Managers
{
    public class RelocalizeManager
    {
        private ISessionAwareCoreService client = null;
        private AppConfigSettings conf = null;
        private static ReadOptions expandedReadOptions = null;

        internal static ReadOptions ExpandedReadOptions
        {
            get
            {
                if (expandedReadOptions == null)
                {
                    expandedReadOptions = new ReadOptions();
                }

                expandedReadOptions.LoadFlags = LoadFlags.Expanded;
                return expandedReadOptions;
            }
        }

        internal RelocalizeManager(ISessionAwareCoreService client, AppConfigSettings conf)
        {
            this.client = client;
            this.conf = conf;
        }

        internal RepositoryLocalObjectData CreateRelocalizedVersion(RepositoryLocalObjectData item, RepositoryLocalObjectData parent)
        {
            // This method will require admin credentials to read parent items in the BluePrint that the current user may not have permissions to.
            // So we impersonate to read them, and then impersonate back.
            client.Impersonate(conf.AdminImpersonationUser); //set back to the session's so we can keep using the client as before.
            //Type t = item.GetType();
            //string tridionItemType = t.FullName;

            TcmUri itemTcmUri = new TcmUri(item.Id);

            switch (itemTcmUri.ItemType)
            {
                case Tridion.ContentManager.ItemType.Component:
                    {
                        var comp = (ComponentData)item;
                        ComponentData parentComp = null;
                        try
                        {
                            parentComp = (ComponentData)client.Read(parent.Id, ExpandedReadOptions);
                        }
                        catch
                        {
                            throw;
                        }

                        comp.Content = parentComp.Content;
                        if (comp.Content != null)
                        {
                            comp.Content = SetToLocalContext(comp.Content, comp.Id, parent.Id);
                        }

                        comp.Metadata = parentComp.Metadata;
                        if (comp.Metadata != null)
                        {
                            comp.Metadata = SetToLocalContext(comp.Metadata, comp.Id, parent.Id);
                        }

                        comp.Title = parentComp.Title;

                        if (parentComp.ComponentType == ComponentType.Multimedia && parentComp.BinaryContent != null)
                        {
                            string filePath = @"C:\Temp\" + Path.GetFileName(parentComp.BinaryContent.Filename);
                            StreamDownloadClient downloadClient = new StreamDownloadClient(conf.DownloadStreamServiceEndpointName);

                            using (Stream stream = downloadClient.DownloadBinaryContentByUser(parentComp.Id, client.GetCurrentUser()))
                            {
                                using (FileStream fs = File.Create(filePath))
                                {
                                    using (MemoryStream ms = new MemoryStream())
                                    {
                                        stream.CopyTo(ms);
                                        byte[] binary = ms.ToArray();
                                        fs.Write(binary, 0, binary.Length);
                                    }
                                }
                            }

                            comp.BinaryContent = new BinaryContentData
                            {
                                Filename = parentComp.BinaryContent.Filename,
                                MultimediaType = parentComp.BinaryContent.MultimediaType,
                                UploadFromFile = filePath
                            };
                        }

                        item = comp;

                        break;
                    }
                case Tridion.ContentManager.ItemType.Page:
                case Tridion.ContentManager.ItemType.Keyword:
                case Tridion.ContentManager.ItemType.Publication:
                case Tridion.ContentManager.ItemType.Folder:
                case Tridion.ContentManager.ItemType.StructureGroup:
                case Tridion.ContentManager.ItemType.Category:
                default:
                    {
                        //copy over only title, metadata
                        throw new NotImplementedException();
                        //break;
                    }
            }
            client.Impersonate(conf.CurrentUser); //impersonate back.

            return item;
        }

        /// <summary>
        /// Takes and string value and replaces any TCM ID values within the string with the ID within the
        /// specified local context
        /// </summary>
        /// <param name="value">The string to have all TCMs within set to local context. This could be an individual field value or the entires Content or Metadata XML.</param>
        /// <param name="localTcmContext">any TCM ID in the local context (to)</param>
        /// <param name="parentTcmContext">any TCM ID in the parent context (from)</param>
        /// <returns></returns>
        internal string SetToLocalContext(string value, string localTcmId, string parentTcmId)
        {
            string returnVal = null;
            if (value != null)
            {
                int parentPubId = new TcmUri(parentTcmId).PublicationId;
                int compPubId = new TcmUri(localTcmId).PublicationId;

                string tcmContextPrefix = "tcm:{0}";
                string compTcmContext = string.Format(tcmContextPrefix, compPubId.ToString());
                string parentTcmContext = string.Format(tcmContextPrefix, parentPubId.ToString());
                returnVal = value.Replace(parentTcmContext, compTcmContext);
            }

            return returnVal;
        }

        internal ItemFieldValue[] SetToLocalContext(ItemFieldValue[] values, string localTcmId, string parentTcmId)
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i].Value = SetToLocalContext(values[i].Value, localTcmId, parentTcmId);
                //look for the linked component's title in the Description field.
                if( values[i].Description != null )
                {
                    values[i].Description = TryGetTitleOfComponentLinkField(values[i].Value);
                }
            }

            return values;
        }

        /// <summary>
        /// Scan up the blueprint to find the immediate parent component.
        /// If the object passed in is shared (not localized) then null is returned.
        /// </summary>
        /// <param name="rlo">The localized item </param>
        /// <returns></returns>
        internal RepositoryLocalObjectData FindParentItem(RepositoryLocalObjectData rlo)
        {
            RepositoryLocalObjectData parentRlo = null;
            var bpi = rlo.BluePrintInfo;

            if (bpi.IsLocalized == true)
            {
                BluePrintChainFilterData filter = new BluePrintChainFilterData();
                filter.Direction = BluePrintChainDirection.Up;
                IdentifiableObjectData[] blueprintScanUp = client.GetList(rlo.Id.ToString(), filter);
                // The returned list of items is in the order of inheritance. 
                // Therefore the zeroth item is the initial localized component and the first is the immediate parent, and so on.
                parentRlo = (RepositoryLocalObjectData)blueprintScanUp[1];
            }

            return parentRlo;
        }

        /// <summary>
        /// Get a field value as specified in the GetParentItemField input model.
        /// 
        /// Currently the method only supports first level fields. Embedded fields are not yet supported.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        internal ItemFieldValue[] GetFieldValue(RepositoryLocalObjectData item, Models.ItemFieldModel model)
        {
            ItemFieldValue[] returnVal = null;

            client.Impersonate(conf.AdminImpersonationUser); //set back to the session's so we can keep using the client as before.
            TcmUri parentUri = new TcmUri(item.Id);
            switch (parentUri.ItemType)
            {
                case Tridion.ContentManager.ItemType.Component:
                    {
                        ComponentData parentComp = null;
                        SchemaFieldsData schemaFields = null;
                        try
                        {
                            parentComp = (ComponentData)client.Read(item.Id, ExpandedReadOptions);
                            schemaFields = client.ReadSchemaFields(parentComp.Schema.IdRef, true, ExpandedReadOptions);

                            // Get Field for Content or for Metadata
                            // Let's start with Content
                            Fields fields = null;
                            if (TridionFieldContextConstants.SchemaBasedFields.Equals(model.FieldContext))
                            {
                                fields = Fields.ForContentOf(schemaFields, parentComp);
                            }
                            else
                            {
                                fields = Fields.ForMetadataOf(schemaFields, parentComp);
                            }

                            var field = fields[model.FieldName];
                            if (field != null)
                            {
                                // If we have a single-value field, else multi-value
                                if (field.Values == null || (field.Values != null && field.Values.Count <= 1))
                                {
                                    returnVal = new ItemFieldValue[1];
                                    ItemFieldValue fv = new ItemFieldValue();
                                    returnVal[0] = fv;
                                    returnVal[0].Value = field.Value;
                                    returnVal[0].Description = TryGetTitleOfComponentLinkField(field.Value);
                                }
                                else
                                {
                                    returnVal = new ItemFieldValue[field.Values.Count];
                                    int i = 0;
                                    foreach (var val in field.Values)
                                    {
                                        ItemFieldValue fv = new ItemFieldValue();
                                        returnVal[i] = fv;
                                        returnVal[i].Value = val;
                                        returnVal[i].Description = TryGetTitleOfComponentLinkField(val);
                                        i++;
                                    }
                                }
                            }
                        }
                        catch
                        {
                            throw;
                        }
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }

            client.Impersonate(conf.CurrentUser); //impersonate back from admin since we're done reading higher levels of the BP.

            return returnVal;
        }

        private string TryGetTitleOfComponentLinkField(string tcmUri)
        {
            string title = null;
            if (tcmUri!=null && tcmUri.StartsWith("tcm:"))
            {
                try
                {
                    TcmUri uri = new TcmUri(tcmUri);
                    var linkedComp = (ComponentData)client.Read(tcmUri, new ReadOptions());
                    title = linkedComp.Title;
                }
                catch (InvalidTcmUriException e)
                {
                    // we do nothing in this case, bacause the field value is not a valid tcm id. It's possible that it's something else that starts with the string "tcm:"
                }
            }
            return title;
        }
    }
}