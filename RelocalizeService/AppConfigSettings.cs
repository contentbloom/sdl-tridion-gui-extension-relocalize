﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RelocalizeService
{
    public class AppConfigSettings
    {
        public string AdminImpersonationUser { get; set; }
        public string CurrentUser { get; set; }
        public string WebServiceEndpointName { get; set; }
        public string DownloadStreamServiceEndpointName { get; set; }
    }
}