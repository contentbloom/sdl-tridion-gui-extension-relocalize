﻿using RelocalizeService.Models;
using System;
using System.ServiceModel;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tridion.ContentManager.CoreService.Client;
using System.Configuration;
using Tridion.ContentManager;
using System.IO;
using System.Web;
using RelocalizeService.Managers;
using System.Web.Script.Serialization;

namespace RelocalizeService.Controllers
{
    [RoutePrefix("api/relocalize")]
    public class RelocalizeController : ApiController
    {
        [HttpPost]
        [Route("relocalizecomponent")]
        public RelocalizeReturnMessage RelocalizeTridionItem(RelocalizeTridionItemModel model)
        {
            AppConfigSettings conf = InitializeConfigSettings();
            RelocalizeReturnMessage returnMessage = new RelocalizeReturnMessage();

            using (SessionAwareCoreServiceClient client = new SessionAwareCoreServiceClient(conf.WebServiceEndpointName))
            {
                client.Impersonate(conf.CurrentUser);

                RepositoryLocalObjectData item = (RepositoryLocalObjectData)client.TryRead(model.ItemId, RelocalizeManager.ExpandedReadOptions);  // Read without checking out.
                if (item != null)
                {
                    var relocalizeManager = new RelocalizeManager(client, conf);
                    RepositoryLocalObjectData parent = relocalizeManager.FindParentItem(item);

                    // Take the XML of the parent item and copy it here.
                    if (parent != null)
                    {
                        try
                        {
                            //// Let's hardcode some shit for easy testing.
                            //// TO-DO: don't forget to remove this crap later.

                            //var testModel = new ItemFieldModel();
                            //testModel.ItemId = "tcm:10-17012";
                            //testModel.FieldName = "image";
                            //testModel.FieldContext = "SchemaBasedFields";

                            //var json = new JavaScriptSerializer().Serialize(testModel);

                            //ItemFieldReturnMessage itemFieldReturnMsg = new ItemFieldReturnMessage();
                            //itemFieldReturnMsg.FieldValues = relocalizeManager.GetFieldValue(parent, testModel);
                            //itemFieldReturnMsg.FieldValues = relocalizeManager.SetToLocalContext(itemFieldReturnMsg.FieldValues, item.Id, parent.Id);
                            //itemFieldReturnMsg.State = 1;
                            //itemFieldReturnMsg.Message = "Successfully retrieved field";

                            //var json2 = new JavaScriptSerializer().Serialize(itemFieldReturnMsg);

                            //// End test code. Remove above.

                            item = relocalizeManager.CreateRelocalizedVersion(item, parent);
                        }
                        //catch (Exception e)
                        //{
                        //    returnMessage.Message = e.Message;
                        //    returnMessage.State = 0;
                        //    return returnMessage;
                        //}
                        catch
                        {
                            throw;
                        }

                        if (client.TryCheckOut(model.ItemId, RelocalizeManager.ExpandedReadOptions) != null)
                        {
                            try
                            {
                                client.Save(item, RelocalizeManager.ExpandedReadOptions);
                                returnMessage.Message = "Item relocalized from parent " + parent.Id;
                                client.CheckIn(id: model.ItemId, removePermanentLock: false, userComment: returnMessage.Message, readBackOptions: RelocalizeManager.ExpandedReadOptions);
                                returnMessage.State = 1;
                            }
                            catch
                            {
                                //returnMessage.Message = e.Message;
                                returnMessage.State = 0;
                                client.UndoCheckOut(model.ItemId, false, RelocalizeManager.ExpandedReadOptions);
                                throw;
                            }
                        }
                    }
                    else
                    {
                        returnMessage.State = 1;
                        returnMessage.Message = "Item is not localized. Nothing to do.";
                    }
                }
            }

            return returnMessage;
        }

        [HttpPost]
        [Route("getparentcomponentfield")]
        public ItemFieldReturnMessage GetParentComponentField(ItemFieldModel model)
        {
            var msg = new ItemFieldReturnMessage();
            AppConfigSettings conf = InitializeConfigSettings();

            using (SessionAwareCoreServiceClient client = new SessionAwareCoreServiceClient(conf.WebServiceEndpointName))
            {
                // Read component with expanded flags.
                // Get parent content.
                // Use item fields helper class to find the field and populate the value.
                client.Impersonate(conf.CurrentUser);

                RepositoryLocalObjectData item = (RepositoryLocalObjectData)client.TryRead(model.ItemId, RelocalizeManager.ExpandedReadOptions);  // Read without checking out.
                if (item != null)
                {
                    var relocalizeManager = new RelocalizeManager(client, conf);
                    RepositoryLocalObjectData parent = relocalizeManager.FindParentItem(item);

                    // Take the XML of the parent item and copy it here.
                    if (parent != null)
                    {
                        // Find the field
                        try
                        {
                            msg.FieldValues = relocalizeManager.GetFieldValue(parent, model);
                            if (msg == null)
                            {
                                msg.State = 0;
                                msg.Message = "Couldn't get parent field for some reason.";
                            }
                            else
                            {
                                msg.FieldValues = relocalizeManager.SetToLocalContext(msg.FieldValues, item.Id, parent.Id);
                                msg.State = 1;
                                msg.Message = "Successfully retrieved field from parent item " + parent.Id;
                            }
                        }
                        catch
                        {
                            msg.State = 0;
                            //msg.Message = e.Message;
                            throw;
                        }
                    }
                    else
                    {
                        // This isn't a scenario that should be called by the GUI Extension anyway, but we'll be nice about treating it if gets called.
                        msg.State = 0;
                        msg.Message = "Item is not localized, so no parent exists.";
                    }
                }
            }

            return msg;
        }
        
        private AppConfigSettings InitializeConfigSettings()
        {
            AppConfigSettings conf = new AppConfigSettings();

            conf.CurrentUser = HttpContext.Current.User.Identity.Name;
            conf.AdminImpersonationUser = ConfigurationManager.AppSettings["adminImpersonationUser"];

            // for backwards compatibility we'll allow specifying endpoints all the way back from 2011. 
            conf.WebServiceEndpointName = ConfigurationManager.AppSettings["webServiceEndpointName"];
            conf.DownloadStreamServiceEndpointName = ConfigurationManager.AppSettings["downloadStreamServiceEndpointName"];

            return conf;
        }
    }
}
