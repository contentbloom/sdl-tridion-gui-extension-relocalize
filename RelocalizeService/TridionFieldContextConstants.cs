﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RelocalizeService
{
    public static class TridionFieldContextConstants
    {
        public const string SchemaBasedFields = "SchemaBasedFields";
        public const string ItemMetadata = "ItemMetadata";
    }
}