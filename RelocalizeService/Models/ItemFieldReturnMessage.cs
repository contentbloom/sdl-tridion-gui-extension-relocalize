﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RelocalizeService.Models
{
    public class ItemFieldReturnMessage
    {
        public int State { get; set; }
        public string Message { get; set; }
        public ItemFieldValue[] FieldValues { get; set; }
    }

    public class ItemFieldValue
    {
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
