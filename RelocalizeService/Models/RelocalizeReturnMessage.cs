﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RelocalizeService.Models
{
    public class RelocalizeReturnMessage
    {
        public int State { get; set; }
        public string Message { get; set; }
    }
}
