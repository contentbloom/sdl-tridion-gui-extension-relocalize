﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RelocalizeService.Models
{
    public class ItemFieldModel
    {
        public string ItemId { get; set; } //the localized component TCM ID
        public string FieldName { get; set; }
        public string FieldContext { get; set; } //content or metadata
    }
}
