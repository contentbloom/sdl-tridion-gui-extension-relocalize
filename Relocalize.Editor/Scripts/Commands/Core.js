﻿Type.registerNamespace("ContentBloom.Relocalize");

ContentBloom.Relocalize.Core = function () {
    Type.enableInterface(this, "ContentBloom.Relocalize.Core");

    this.addInterface("Tridion.Cme.Command", ["Relocalize", $const.AllowedActions.UnLocalize]);

    //add confirm handler
    $messages.addEventListener("confirm", this.getDelegate(this.msgEventHandler));

};

ContentBloom.Relocalize.Core.prototype._isEnabled = function (selection, undefined) {

    if (selection) {
        //components only
        if (selection.getItemType(0) == $const.ItemType.COMPONENT) {
            //only if in state of unlocalise
            if (this.isActionEnabled(selection,$const.AllowedActions.UnLocalize)) {
                //only when selected one item
                if (selection.getCount() == 1) {
                    return true;
                }
            }
        }
    } 
    return false;
};

ContentBloom.Relocalize.Core.prototype._execute = function (selection) {

    if (selection.getCount() == 1) {

        var id = selection.getItem(0);
        var item = $models.getItem(id);
        var title = item.getTitle() || item.getStaticTitle();

        var msg = $messages.registerQuestion(Relocalize$Core$Data.TextConfirmRelocalizeMessage, Tridion.Utils.String.format(Relocalize$Core$Data.TextConfirmRelocalizeTitle, title, id), null, true);

        //set message options (we'll 'get' later)
        msg.setOption("CommandCallbackType", "RelocalizeCommandCallback");
        msg.setOption("itemId", Object.serialize(id));
        msg.setOption("itemTitle", title);
    }

    return ;
};

ContentBloom.Relocalize.Core.prototype.msgEventHandler = function (e) {

    var msgId = e.data.messageID;
    var msg = $messages.getMessageByID(msgId);
    if (msg) {
        switch (msg.getOption("CommandCallbackType")) {
            case "RelocalizeCommandCallback":
                msg.setOption("CommandCallbackType", null);
                switch (e.type) {
                    case "confirm":

                        var itemId = Object.deserialize(msg.getOption("itemId"));
                        var itemTitle = msg.getOption("itemTitle");
                        
                        //clear options
                        msg.setOption("itemId", null);
                        msg.setOption("itemTitle", null);
                        
                        //format into JSON
                        var data = "{ ItemId: '" + itemId + "'}";
                        
                        //tell them we're making the call
                        $messages.registerNotification(Tridion.Utils.String.format(Relocalize$Core$Data.TextNotificationRelocalizing, itemId, itemTitle), null, false, false)

                        //make the ajax call then update user on status
                        $relocalizeUtils.makeAjax(
                            'POST',
                            '/RelocalizeService/api/relocalize/relocalizecomponent',
                            data,
                            function (data) {

                                //work out if successful
                                var isSuccessful = false;
                                var message = null;

                                if (data != null) {
                                    if (data.State != null) {
                                        if (data.State == "1") {
                                            isSuccessful = true;
                                        }
                                    }
                                    if (data.Message != null) {
                                        message = data.Message;
                                    }
                                }

                                //show msg accordingly
                                if (isSuccessful) {
                                    var successMessage = "Your item was successfully relocalized.";
                                    if (message != null) {
                                        successMessage = message;
                                    }
                                    $relocalizeUtils.ShowSuccessMessage(itemId, itemTitle, successMessage)
                                }
                                else {
                                    var errorMessage = "Error returning status of true. Please contact your administrator.";
                                    if (message != null) {
                                        errorMessage = message;
                                    }
                                    $relocalizeUtils.ShowErrorMessage(itemId, itemTitle, errorMessage);
                                }
                            },
                            function (data) {
                                //Error callback
                                $relocalizeUtils.ShowErrorMessage(itemId, itemTitle, Relocalize$Core$Data.TextWebserviceError);
                            },
                            null
                        )
                        break;
                }
                break;
        }
    }
};