﻿
Type.registerNamespace("ContentBloom.Relocalize");


//------------------------------------------
// Handle click of a field relocalize button
//------------------------------------------

function Relocalize$RelocalizeFieldClickHandler(e){

    self = $j(e);

        var item = $display.getItem()

        //only enabled if editable
        if (!item.isReadOnly()) {

            //gather data from element
            var itemId = item.getId();
            var fieldName = self.attr("data-field-name");
            var fieldNameFull = self.attr("data-field-name-full");
            var fieldContext = self.attr("data-field-context");
            var fieldIsMultivalue = self.attr("data-field-is-multivalue");

            //my parent
            parent = self.closest(".field")
            //my position in siblings
            var fieldOrdinal = parent.find(".relocalize-field").index(self);

            // Un relocalize (put back orignal value)
            //------------------------------
            if (self.hasClass("relocalized")) {

                //get the original data from the collection
                var fieldUniqueId = self.attr("data-field-unique-id");
                var originalData = AssociativeArray.Get(Relocalize$DisplayEvents$Data.OriginalFieldValues, fieldUniqueId);

                //ask them to confirm
                var msg = $messages.registerQuestion(
                    Tridion.Utils.String.format(Relocalize$Core$Data.TextConfirmUnRelocalizeTitle, fieldNameFull),
                    Relocalize$Core$Data.TextConfirmUnRelocalizeMessage, null, true
                );

                //set message properties
                msg.setOption("RelocalizeCommandCallbackType", "UndoRelocalizeCommandCallback");
                Relocalize$setMessageOptions(msg, itemId, fieldName, fieldContext, fieldOrdinal, fieldIsMultivalue);

                // add original field value to message box
                $relocalizeUtils.AddTextareaToMessageBox(".Description", "relocalize-code-text-area", originalData);
            }

                // Relocalize (call webservice)
                //------------------------------
            else {

                //we'll pass this json
                var data = "{ ItemId: '" + itemId + "', FieldName: '" + fieldName + "', FieldContext: '" + fieldContext + "'}";

                //get the value from the webservice
                //make the ajax call then update user on status
                $relocalizeUtils.makeAjax(
                    'POST',
                    '/RelocalizeService/api/relocalize/getparentcomponentfield',
                    data,
                    function (data) {

                        //work out if successful
                        var isSuccessful = false;
                        var message = null;

                        if (data != null) {
                            if (data.State != null) {
                                if (data.State == "1") {
                                    isSuccessful = true;
                                }
                            }
                            if (data.Message != null) {
                                message = data.Message;
                            }
                        }

                        if (isSuccessful) {
                            if (data.FieldValues != null) {

                                if (data.FieldValues.length > 0) {

                                    var msg = null;

                                    //diff messages for one or more                        
                                    if (data.FieldValues.length == 1) {

                                        //ask them to confirm
                                        msg = $messages.registerQuestion(
                                            Tridion.Utils.String.format(Relocalize$Core$Data.TextConfirmRelocalizeFieldTitle, fieldNameFull),
                                            Relocalize$Core$Data.TextConfirmRelocalizeFieldMessage, null, true
                                        );
                                        $relocalizeUtils.AddTextareaToMessageBox(".Description", "relocalize-code-text-area", Relocalize$getMessageFromFieldValue(data.FieldValues[0]));
                                    }
                                    else {
                                        msg = $messages.registerWarning(
                                            Tridion.Utils.String.format(Relocalize$Core$Data.TextConfirmRelocalizeFieldByChoiceTitle, fieldNameFull),
                                            Relocalize$Core$Data.TextConfirmRelocalizeFieldByChoiceMessage, null, true
                                        );
                                        for (var i = 0; i < data.FieldValues.length; i++) {
                                            // add all field values to message box so they can choose one
                                            $relocalizeUtils.AddTextareaToMessageBox(".Description", "relocalize-code-text-area", Relocalize$getMessageFromFieldValue(data.FieldValues[i]));

                                            //show the content to replace with
                                            $j(".messagebox .Description").append(
                                                '<div class="relocalize-select-button tridion button" onclick="Relocalize$RelocalizeSelectButtonHandler(this)" data-ordinal="' + i + '" data-related-message-id="' + msg.getId() + '"><span class="text">Select</span></div>'
                                            );
                                        }
                                    }
                                    //set message options (we'll use these in the call back)
                                    msg.setOption("RelocalizeCommandCallbackType", "RelocalizeCommandCallback");
                                    Relocalize$setMessageOptions(msg, itemId, fieldName, fieldContext, fieldOrdinal, fieldIsMultivalue);

                                    //use global var for data array
                                    Relocalize$DisplayEvents$Data.FieldValues = data.FieldValues;
                                }
                            }
                        }
                        else {
                            var errorMessage = "Error obtaining parent field value. Please contact your administrator.";
                            if (message != null) {
                                errorMessage = message;
                            }
                            $relocalizeUtils.ShowErrorMessage(itemId, fieldName, errorMessage);
                        }
                    },
                    function (data) {
                        //Error callback
                        $relocalizeUtils.ShowErrorMessage(itemId, fieldName, Relocalize$Core$Data.TextWebserviceError);
                    },
                    null
                )

            }
        }
}

//------------------------------------------
// Handle click of a relocalize select button
//------------------------------------------
function Relocalize$RelocalizeSelectButtonHandler(e)
{

    //remove the class from all elements then add it to me only
    var buttons = document.getElementsByClassName("relocalize-select-button");
    for (var c = 0; c < buttons.length; c++) {
        var button = buttons[c];
        button.className = button.className.replace('selected', '');
    }
    e.className = e.className + " selected";

    var view = $display.getView();
    if (view) {
            
        //get the properties
        var dataOrdinal = e.getAttribute("data-ordinal");
        var dataRelatedMessageId = e.getAttribute("data-related-message-id");
        
        //get the message
        var msg = $messages.getMessageByID(dataRelatedMessageId);

        //gather options from the msg
        var msgValues = $relocalizeUtils.GetFieldOptionsFromMessage(msg);

        var fieldValue = msgValues.fieldValues[dataOrdinal];

        if (typeof(fieldValue) != 'undefined') {

            //get the fieldbuilder
            var fieldBuilder = $relocalizeUtils.GetFieldBuilderByContext(msgValues.fieldContext);

            if (fieldBuilder != null) {

                //get the field
                var field = fieldBuilder.getField(msgValues.fieldName)
                var fieldElm = $j(field.getElement());

                if ((field.isMultivalued()) && (field.getListType() != "checkbox") && (field.getListType() != "select")) {
                    //override the field to the mukti value version
                    fieldElm = field.getElement();
                    cntrls = $j('div.input', fieldElm.parentNode);
                    field = cntrls[msgValues.fieldOrdinal].control;
            
                    // update the context
                    var relocalizeField = $j(fieldElm).parent().find(".relocalize-field").eq(msgValues.fieldOrdinal);
                }
                else {

                    var relocalizeField = fieldElm.parent(".field").prev(".relocalize-field");
                }
                //store the original field value then update field
                AssociativeArray.Push(Relocalize$DisplayEvents$Data.OriginalFieldValues, Relocalize$DisplayEvents$Data.UniqueFieldId, field.getValues());

                //try set value and update context if successful
                if ($relocalizeUtils.SetFieldValue(field, fieldValue.Value)) {
                    relocalizeField.addClass("relocalized");

                    //add attr to element + increment count
                    relocalizeField.attr("data-field-unique-id", Relocalize$DisplayEvents$Data.UniqueFieldId);
                    Relocalize$DisplayEvents$Data.UniqueFieldId++;
                }
            }
        }
            
    }
    msg.doArchive();
}
 



//-------------------
// LOCAL HELPERS
//-------------------
function Relocalize$setMessageOptions(msg, itemId, fieldName, fieldContext, fieldOrdinal, fieldIsMultivalue) {
    msg.setOption("itemId", itemId);
    msg.setOption("fieldName", fieldName);
    msg.setOption("fieldContext", fieldContext);
    msg.setOption("fieldOrdinal", fieldOrdinal);
    msg.setOption("isMultivalue", fieldIsMultivalue);
}

function Relocalize$getMessageFromFieldValue(fieldValue) {
    msg = "";
    if (typeof (fieldValue) != 'undefined' && fieldValue != null) {
        if (fieldValue.Value != 'undefined') {
            msg += fieldValue.Value;
        }
        if (fieldValue.Description != null) {
            msg += " (" + fieldValue.Description + ")";
        }
    }
    return msg;
}
 