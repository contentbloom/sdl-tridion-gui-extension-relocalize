﻿
Type.registerNamespace("ContentBloom.Relocalize");

if ($display) {

    (function () {
    
        //---------------
        //Attach to events
        //---------------
        //Listen for message confirm 
        $messages.addEventListener("confirm", Relocalize$messageConfirmHandler);

        //Listen for display start 
        $evt.addEventHandler($display, "start", Relocalize$onDisplayStartedReadonly);

        //---------------
        // Event handlers
        //---------------
        
        //Handle : Display start (read only)
        function Relocalize$onDisplayStartedReadonly() {

            //remove the handler
            $evt.removeEventHandler($display, "start", Relocalize$onDisplayStartedReadonly);

            var view = $display.getView();
            if (view) {

                // Content fields
                //-------------------
                var fieldBuilder = view.properties.controls.fieldBuilder;
                $evt.addEventHandler(fieldBuilder, "load", Relocalize$onDisplayStartedReadonly$_ModifyUI);

                // Metadata fields
                //-------------------
                var metaFieldBuilder = $relocalizeUtils.GetMetadataFieldbuilder();
                if (metaFieldBuilder != null) {
                    $evt.addEventHandler(metaFieldBuilder, "load", Relocalize$onDisplayStartedReadonly$_ModifyUI);
                }
            }
        
        };

        //Handler : Fields loaded
        //Let's put the icons next to each field
        function Relocalize$onDisplayStartedReadonly$_ModifyUI(event) {

            var fieldBuilder = event && event.source;
            if (fieldBuilder) {

                //get the item
                var item = $display.getItem();

                //only process for inherited items that are editable
                if (($relocalizeUtils.IsLocalToContextPublication(item) == false) && (item.isReadOnly() == false)) {


                    //handle the insert of a field
                    $evt.addEventHandler(fieldBuilder, "insert", Relocalize$handleFieldAction$_Insert);

                    //get all fields out of the builder
                    var componentFields = $relocalizeUtils.GetFields(fieldBuilder);

                    var fieldsContainer = fieldBuilder.properties.input;
                    var fieldsNode = fieldsContainer.getElement();

                    $j(fieldsNode).children().each(function (index, elm) {

                        //matching component item (we got earlier)
                        componentItem = componentFields[index];
                        var field = componentItem.fieldObject;

                        //handle all fields but the following
                        if (Relocalize$DisplayEvents$Data.UnsupportedFieldTypes.indexOf(field.getTypeName()) < 0) {

                            if ((field.isMultivalued()) && (field.getListType() != "checkbox") && (field.getListType() != "select")) {

                                //multivalued so we may need to add button multiple times
                                $j(elm).children('.input-with-bar').each(function (index, e) {

                                    $j(this).addClass("has-relocalize-field");

                                    var relocalizeActionElement = $relocalizeUtils.GetRelocalizeElement(fieldBuilder.getId(), componentItem.fieldDescription, componentItem.fieldName, true);
                                    e.insertBefore(relocalizeActionElement, e.firstChild)
                                });
                            }
                            else {
                                $j(elm).addClass("has-relocalize-field");

                                var relocalizeActionElement = $relocalizeUtils.GetRelocalizeElement(fieldBuilder.getId(), componentItem.fieldDescription, componentItem.fieldName, false);
                                elm.parentNode.insertBefore(relocalizeActionElement, elm)
                            }
                        }
                    });

                }
            }
        };

        //Handle : Field insert 
        function Relocalize$handleFieldAction$_Insert(event) {

            //make our logic run after the insert event as the 'next' (new) field will have been added by then
            setTimeout(function () {

                var fieldBuilder = event && event.source;
                if (fieldBuilder) {

                    //get the element 
                    fieldObj = window.nextSibling = event.data.field.getNextFieldSibling();
                    fieldElm = fieldObj.getElement();

                    //relocalize not yet added to this field, so add it
                    if (fieldElm.className.indexOf("has-relocalize-field") < 0) {

                        //gather properties
                        fieldContext = fieldBuilder.getId();
                        fieldName = fieldObj.getFieldName();
                        parentField = fieldObj.getParentField();

                        fieldNameFull = "";
                        var cntrl = $j('div.input', fieldElm.parentNode)[0].control;
                        var label = $j('label', fieldElm.parentNode).eq(0)
                        if (label.length > 0) {
                            fieldNameFull = label.attr("title");
                        }
                        
                        //dont allow on certain field types
                        if (Relocalize$DisplayEvents$Data.UnsupportedFieldTypes.indexOf(cntrl.getTypeName()) < 0 && typeof (parentField) == 'undefined') {                       

                            //add the class
                            fieldElm.className = fieldElm.className + " has-relocalize-field";

                            //create the node 
                            var relocalizeActionElement = $relocalizeUtils.GetRelocalizeElement(fieldContext, fieldNameFull, fieldName, cntrl.isMultivalued());
                            
                            // prepend relocalize element to the beginning of the field
                            fieldElm.insertBefore(relocalizeActionElement, fieldElm.firstChild)
                        }
                    }
                }

            }, 0);

        }
        
        //Handle : Message confirm 
        function Relocalize$messageConfirmHandler(e) {
            
            var view = $display.getView();
            if (view) {

                //get the message
                var msgId = e.data.messageID;
                var msg = $messages.getMessageByID(msgId);

                if (msg) {

                    //gather options from the msg
                    var msgValues = $relocalizeUtils.GetFieldOptionsFromMessage(msg);
                    $relocalizeUtils.ClearFieldOptionsFromMessage(msg);

                    //get the fieldbuilder
                    var fieldBuilder = $relocalizeUtils.GetFieldBuilderByContext(msgValues.fieldContext);

                    if (fieldBuilder != null) {

                        switch (msg.getOption("RelocalizeCommandCallbackType")) {

                            // Relocalize
                            //------------------------------
                            case "RelocalizeCommandCallback":

                                //clear the value
                                msg.setOption("RelocalizeCommandCallbackType", null);

                                switch (e.type) {
                                    case "confirm":

                                        if (msgValues.fieldValues.length > 0) {

                                            //get the first from the data (it'll never be multi value, that's taken car of in diff handler)
                                            var fieldValue = msgValues.fieldValues[0];

                                            if (typeof(fieldValue) != 'undefined') {

                                                //convert nullable to a string (.setValues can handle "null" but not null)
                                                if (fieldValue.Value == null) {
                                                    fieldValue.Value = "null";
                                                }

                                                //get the field
                                                var field = fieldBuilder.getField(msgValues.fieldName)
                                                var fieldElm = $j(field.getElement());

                                                // multivalue fields
                                                if ((field.isMultivalued()) && (field.getListType() != "checkbox") && (field.getListType() != "select")) {
                                                    //override the field to the mukti value version
                                                    fieldElm = field.getElement();
                                                    cntrls = $j('div.input', fieldElm.parentNode);
                                                    field = cntrls[msgValues.fieldOrdinal].control;

                                                    // update the context
                                                    var relocalizeField = $j(fieldElm).parent().find(".relocalize-field").eq(msgValues.fieldOrdinal);
                                                }
                                                // single value fields
                                                else {
                                                    // update the context
                                                    var relocalizeField = fieldElm.parent(".field").prev(".relocalize-field");
                                                }

                                                //store the original field value then update field
                                                AssociativeArray.Push(Relocalize$DisplayEvents$Data.OriginalFieldValues, Relocalize$DisplayEvents$Data.UniqueFieldId, field.getValues());

                                                //try set value and update context if successful
                                                if ($relocalizeUtils.SetFieldValue(field, fieldValue.Value)) {
                                                    relocalizeField.addClass("relocalized");

                                                    //add attr to element + increment count
                                                    relocalizeField.attr("data-field-unique-id", Relocalize$DisplayEvents$Data.UniqueFieldId);
                                                    Relocalize$DisplayEvents$Data.UniqueFieldId++;
                                                }
                                            }
                                        }
                                        break;
                                }
                                break;

                            // UndoRelocalize
                            //------------------------------

                            case "UndoRelocalizeCommandCallback":

                                //clear the value
                                msg.setOption("RelocalizeCommandCallbackType", null);

                                switch (e.type) {
                                    case "confirm":

                                        //get the field
                                        var field = fieldBuilder.getField(msgValues.fieldName)
                                        var fieldElm = $j(field.getElement());

                                        if ((field.isMultivalued()) && (field.getListType() != "checkbox") && (field.getListType() != "select")) {
                                            //override the field to the mukti value version
                                            fieldElm = field.getElement();
                                            cntrls = $j('div.input', fieldElm.parentNode);
                                            field = cntrls[msgValues.fieldOrdinal].control;

                                            // update the context
                                            var relocalizeField = $j(fieldElm).parent().find(".relocalize-field").eq(msgValues.fieldOrdinal);
                                        }
                                        else {
                                            // update the context
                                            var relocalizeField = fieldElm.parent(".field").prev(".relocalize-field");
                                        }

                                        //get the original content and set in field
                                        var originalData = AssociativeArray.Get(Relocalize$DisplayEvents$Data.OriginalFieldValues, relocalizeField.attr("data-field-unique-id"));

                                        //try set value and update context if successful
                                        if ($relocalizeUtils.SetFieldValue(field, originalData)) {
                                            //set class context
                                            relocalizeField.removeClass("relocalized");
                                        }
                                       
                                        break;
                                }
                                break;
                        }
                    }
                }
            }
        }
    })();
}

