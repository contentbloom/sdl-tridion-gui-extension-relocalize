﻿Type.registerNamespace("ContentBloom.Prototype.RelocalizeExtensions");


//************************
// Utility Helpers
//************************

//---------------------------------------
// Standard methods (using Anguilla only)
//---------------------------------------

ContentBloom.Prototype.RelocalizeExtensions.Utilities = function Utilities() {
};

ContentBloom.Prototype.RelocalizeExtensions.Utilities.ShowSuccessMessage = function (id, title, description) {
    $messages.registerGoal("Relocalizing Successful: \"" + id + "\" (" + title + ")", description, false, false)
};

ContentBloom.Prototype.RelocalizeExtensions.Utilities.ShowErrorMessage = function (id, title, description) {
    $messages.registerError("Relocalizing Failed: \"" + id + "\" (" + title + ")", description, null, false, false)
};

ContentBloom.Prototype.RelocalizeExtensions.Utilities.IsLocalToContextPublication = function (item) {
    if (item.isLocalized() == false && item.isShared() == false) {
        return true
    }
    else {
        return false
    }
};

ContentBloom.Prototype.RelocalizeExtensions.Utilities.GetRelocalizeElement = function (fieldContext, fieldNameFull, fieldName, isMultiValued) {

    //create node to add
    var element = document.createElement("span");
    //set attributes
    element.setAttribute("data-field-context", fieldContext);
    element.setAttribute("data-field-name-full", fieldNameFull);
    element.setAttribute("data-field-name", fieldName);
    element.setAttribute("data-field-is-multivalue", isMultiValued);
    element.title = "Relocalize";
    element.className = "relocalize-field";
    element.setAttribute("onclick", "Relocalize$RelocalizeFieldClickHandler(this);")

    return element;
};

ContentBloom.Prototype.RelocalizeExtensions.Utilities.SetFieldValue = function (field, value) {

    var valid = true
    if (value == null || value == "null") {
        if (Relocalize$DisplayEvents$Data.NonNullableFieldTypes.indexOf(field.getTypeName()) > -1) {
            valid = false;
            msg = $messages.registerWarning(
                Relocalize$Core$Data.TextNonNullableTypeTitle,
                Relocalize$Core$Data.TextNonNullableTypeMessage, null, true
            );
            
        }        
    }

    //handle specific fields differently
    if (field.getTypeName() == "Tridion.FieldBuilder.FieldTypeHandlers.KeywordField") {
        //rejig my properties to allow modification of field
        field.properties.input.settings = field.properties.input.properties.settings;
    }


    //only process if valid
    if (valid) {
        //handle specific fields differently in the null case
        if ((value == null || value == "null") &&
            field.getTypeName() == "Tridion.FieldBuilder.FieldTypeHandlers.MultimediaLinkField" ||
            field.getTypeName() == "Tridion.FieldBuilder.FieldTypeHandlers.ComponentLinkField") {
            field.setValues([]);
        }
        else {
            //all others
            if (Object.prototype.toString.call(value) === '[object Array]') {
                field.setValues(value);
            }
            else {
                field.setValues([value]);
            }
        }
    }
    
    return valid;
}

ContentBloom.Prototype.RelocalizeExtensions.Utilities.GetFieldBuilderByContext = function (fieldContext) {
    var fieldBuilder = null;
    if (fieldContext == "SchemaBasedFields") {
        fieldBuilder = $display.getView().properties.controls.fieldBuilder;
    }
    if (fieldContext == "ItemMetadata") {
        fieldBuilder = $relocalizeUtils.GetMetadataFieldbuilder();
    }
    return fieldBuilder;
}

ContentBloom.Prototype.RelocalizeExtensions.Utilities.GetFields = function (fieldBuilder) {

    //we'll gather our fields based on those in the definition
    fieldDefinitions = fieldBuilder.properties.definitionRoot.children

    //temp values
    var tempFieldDescription = null;;
    var tempFieldName = null;;

    var componentFields = [];

    if (fieldDefinitions.length > 0) {
        var fields = fieldDefinitions[0];
        if (typeof (fields) != 'undefined') {
            children = fields.children;
            //get all children (the fields)
            for (var i = 0 ; i < children.length ; i++) {

                //the field
                var field = children[i];

                if (typeof (field) != 'undefined') {

                    subChildren = field.children;
                    for (var j = 0; j < subChildren.length; j++) {

                        child = field.children[j];

                        if (child.nodeName == "tcm:Name") {
                            tempFieldName = child.innerHTML;
                        }
                        if (child.nodeName == "tcm:Description") {
                            tempFieldDescription = child.innerHTML;
                        }
                    }

                    //get the field using the builder
                    fieldObj = fieldBuilder.getField(tempFieldName);

                    //push the values into our collection
                    if (typeof (fieldObj) != 'undefined') {
                        componentFields.push({
                            fieldObject: fieldObj,
                            fieldName: tempFieldName,
                            fieldDescription: tempFieldDescription
                        });

                    }
                }
            }
        }
    }
    return componentFields;

};

ContentBloom.Prototype.RelocalizeExtensions.Utilities.GetFieldOptionsFromMessage = function(msg) {
    return tempValues = {
        itemId: msg.getOption("itemId"),
        fieldName: msg.getOption("fieldName"),
        fieldContext: msg.getOption("fieldContext"),
        fieldOrdinal: msg.getOption("fieldOrdinal"),
        isMultiValue: msg.getOption("isMultivalue"),
        fieldValues: Relocalize$DisplayEvents$Data.FieldValues
    }
}

ContentBloom.Prototype.RelocalizeExtensions.Utilities.ClearFieldOptionsFromMessage = function(msg) {
    msg.setOption("itemId", null);
    msg.setOption("fieldName", null);
    msg.setOption("fieldContext", null);
    msg.setOption("fieldOrdinal", null);
    msg.setOption("isMultivalue", null);
    Relocalize$DisplayEvents$Data.FieldValues = null;
}

//--------------------------
// Following rely on JQuery
//--------------------------
ContentBloom.Prototype.RelocalizeExtensions.Utilities.makeAjax = function (type, url, data, successCallBack, errorCallBack, completeCallBack) {

    $j.ajax({
        type: type,
        url: url,
        data: ((typeof data !== 'undefined') ? data : '{ }'),
        contentType: "application/json",
        dataType: "json",
        success: function (msg) {
            if (successCallBack != null) {
                successCallBack(msg);
            }
        },
        error: function (msg) {
            if (errorCallBack != null) {
                errorCallBack(msg);
            }
        },
        //runs after error and success callbacks
        complete: function (msg) {
            if (completeCallBack != null) {
                completeCallBack(msg);
            }
        }
    });
};

ContentBloom.Prototype.RelocalizeExtensions.Utilities.AddTextareaToMessageBox = function (elementSelector, className, message) {
    //show the content to replace with
    $j(".messagebox " + elementSelector).append(
        '<textarea readonly class="'+className+'">' +
            message +
        '</textarea>'
    );
};


//-----------------------------------------
// Thanks to Alex Klock for his TREX answer
//-----------------------------------------
ContentBloom.Prototype.RelocalizeExtensions.Utilities.GetMetadataFieldbuilder = function () {
    var fieldBuilder;
    var tabs = $display.getView().properties.controls.TabControl,
    cardCount = tabs.properties.cards.length,
    fieldBuilder,
    card;
    while (cardCount--) {
        card = tabs.properties.cards[cardCount];
        if (card.getId() === "MetadataTab") {
            if (card.isInitialized() == false) {
                card.initialize();
            }
            fieldBuilder = card.properties.controls.fieldBuilder;
        }
    }
    return fieldBuilder;
};


//--------------------------
// new up the utils
//----------------------------
var $relocalizeUtils = ContentBloom.Prototype.RelocalizeExtensions.Utilities;


//************************
// global 'object' helpers
//************************

var Relocalize$Core$Data = {
    //TODO: Get text from global resource 
    TextConfirmRelocalizeTitle: "If you relocalize item \"{0}\" ({1}), a copy of its parent will be created as a new localized version. You can always revert to the previous localized version. Proceed?",
    TextConfirmRelocalizeMessage: "Are you sure you want to relocalize?",
    TextNotificationRelocalizing: "Relocalizing Item: \"{0}\" ({1})...",
    TextWebserviceError: "Error calling the webservice. Please contact your administrator.",
    TextConfirmUnRelocalizeTitle: "Are you sure you want to revert the field \"{0}\"?",
    TextConfirmUnRelocalizeMessage: "Your changes will be reverted to the following:",
    TextConfirmRelocalizeFieldTitle: "Are you sure you want to relocalize the field \"{0}\"?",
    TextConfirmRelocalizeFieldMessage: "Your local changes will be replaced with the following:",
    TextConfirmRelocalizeFieldByChoiceTitle: "Are you sure you want to relocalize the field \"{0}\"?",
    TextConfirmRelocalizeFieldByChoiceMessage: "Parent contains multiple values, please select one from the following:",
    TextNonNullableTypeMessage: "This field does not support empty values, please update manually.",
    TextNonNullableTypeTitle: "Unable to relocalize: Content is empty",
    
    UniqueFieldId: 1
}

var Relocalize$DisplayEvents$Data = {
    OriginalFieldValues: {},
    UniqueFieldId: 1,
    FieldValues: null,
    UnsupportedFieldTypes: [
        "Tridion.FieldBuilder.FieldTypeHandlers.EmbeddedSchemaField"
    ],
    NonNullableFieldTypes: [
        "Tridion.FieldBuilder.FieldTypeHandlers.NumberField",
        "Tridion.FieldBuilder.FieldTypeHandlers.DateField"
    ]
}

var AssociativeArray = {
    Get: function (array, key) {
        if (typeof array == "object") {
            if (typeof array[key] != 'undefined') {
                return array[key];
            }
        }
        return null;
    },
    Push: function (array, key, value) {
        if (typeof array == "object") {
            array[key] = value;
        }
    }
}
