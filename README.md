# SDL Tridion GUI Extension - Relocalize

**Purpose:** Allow content editors to relocalize a component or component field to the value from the inherited parent.

**Version** 1.0 HACKATHON RELEASE 

**Supports:** SDL Tridion 2013 SP1

**Licence:** Apache 2.0 

![IMG_13052015_004756.png](https://bitbucket.org/repo/6XGLg7/images/2637319724-IMG_13052015_004756.png)

## Background 
Content Editors often want to reset their localized components back to the parent version.  To do this they currently have two options:

1.  Manually copy/paste each field from the parent component to the localized component. The pro is that you keep all your localized versions and simply have a new localized version which is a copy of the parent.  The con is that it is a monotonous and time-consuming task.
2.  Use the Unlocalize option in Tridion.  The pro is that this is quick and easy, but the major con is that you lose all your localized versions.

## What it does
The extension adds the ability to relocalize a Tridion Component without losing localized versions.  The solution consists of two parts:

**1.  Relocalize the entire component** - A new option in the Blueprinting section of the Ribbon and Context menus to relocalize the entire Component based on parent.

![relocalize-component.PNG](https://bitbucket.org/repo/6XGLg7/images/1888704109-relocalize-component.PNG)
![relocalize-component-dialog.PNG](https://bitbucket.org/repo/6XGLg7/images/1172068799-relocalize-component-dialog.PNG)
![relocalize-component-success.PNG](https://bitbucket.org/repo/6XGLg7/images/3107477855-relocalize-component-success.PNG)

**2.  Relocalize individual component fields** - A "Copy parent value" button next to each component field allows peaking at the parent's value and copying it into the field.  This allows re-localizing only certain fields.

Clicking the button again provides the option to revert back to your local copy.

![relocalize-component-field.PNG](https://bitbucket.org/repo/6XGLg7/images/3948135752-relocalize-component-field.PNG)
![relocalize-component-field-dialog.PNG](https://bitbucket.org/repo/6XGLg7/images/3526551414-relocalize-component-field-dialog.PNG)

Initial version developed by Content Bloom (http://www.contentbloom.com)

**Contributors:**

 * Jonathan Primmer 
 * Nickoli Roussakov

**Changes in v1.0 HACKATHON RELEASE **
 - Development release

**Please note this extension has been tested on Tridion 2013 GA and 2013 SP1.**

##Installation Instructions
1. Clone repository to a directory on your local machine
2. Open and build Relocalize.sln 
3. From the local machine's directory copy the Relocalize.Editors folder to %TridionHome%\web\WebUI\Editors\.
4. In IIS, create a new virtual directory under the "%TridionHome%\web\WebUI\WebUI\Editors\" with an alias "Relocalize.Editor" and physical path from Step (3). Should be: "%TridionHome%\web\WebUI\Editors\Relocalize.Editor".
5. Modify your %TridionDir%\web\WebUI\WebRoot\Configuration\System.config by inserting the text from the file in /\Relocalize.Editor\Configuration\Editor.config into the <editors> section in System.config.
6. Increment your SDL Tridion System.Config modification attribute 
7. From the local machine's directory (step 1) copy the RelocalizeService folder to "%TridionHome%\web\".
8. In IIS, under the "SDL Tridion" web root, add a new application. Note: make sure it is a child under the "SDL Tridion" application. Give it the alias "RelocalizeService" and a physical path pointing to "%TridionHome%\web\RelocalizeService" as per step 7.
9. Configure settings for the RelocalizerService by editing its **web.config** and specifying the *adminImpersonationUser* and *service endpoint configurations* (just like for any other .NET Web Service app).
10. That's it. Try relocalizing a localized component.

## Issues and next steps:
 * Embeddable fields are not yet supported for individual field relocalization.
 * When relocalizing individual multi-value Component Link fields, if the number of values is not the same as in the parent then can only relocalize for the fields that are present in the current version. To work around this, add a dummy field and then relocalize it.
 * Keywordfields using a multi-value select list are not supported for individual field relocalization.
 * The extension currently isn't supported for Tridion 2013SP1-HR1. There is currently a change in how Tridion 2013SP1-HR1 handles the DOM elements in the CME, so further work is needed to determine the exact was to fix this in a way so that it remains backwards compatible.